#include "Globals.h"
#include "Application.h"
#include "ModuleSceneKen.h"
#include "ModuleRender.h"
#include "ModuleTextures.h"
#include "ModulePlayer.h"
#include "ModuleInput.h"
#include "ModuleAudio.h"
#include "ModuleFadeToBlack.h"
#include "SDL/include/SDL.h"

// Reference at https://youtu.be/OEhmUuehGOA?t=72

ModuleSceneKen::ModuleSceneKen(bool start_enabled) : Module(start_enabled)
{
	// ground
	ground.x = 8;
	ground.y = 391;
	ground.w = 896;
	ground.h = 72;

	// TODO 2 : setup the foreground (red ship) with
	// coordinates x,y,w,h from ken_stage.png
	foreground.x = 8;
	foreground.y = 33;
	foreground.w = 528;
	foreground.h = 206;
	// Background / sky
	background.x = 72;
	background.y = 208;
	background.w = 768;
	background.h = 176;

	// flag animation
	flag.frames.push_back({848, 208, 40, 40});
	flag.frames.push_back({848, 256, 40, 40});
	flag.frames.push_back({848, 304, 40, 40});
	flag.speed = 0.08f;

	// TODO 4: Setup Girl Animation from coordinates from ken_stage.png
	girl.x = 200;
	girl.y = 134;
	girl.w = 232;
	girl.h = 190;

	girl_animation.frames.push_back({ 625, 16, 32, 56 });
	girl_animation.frames.push_back({ 625, 80, 32, 56 });
	girl_animation.frames.push_back({ 625, 144, 32, 56 });
	girl_animation.speed = 0.05f;
	
	//Red ship coordinates
	wave_speed = 0.1f;
	wave_height = 5;

}

ModuleSceneKen::~ModuleSceneKen()
{

}

// Load assets
bool ModuleSceneKen::Start()
{
	LOG("Loading ken scene");
	
	graphics = App->textures->Load("ken_stage.png");
	red_ship = new Ship(wave_height, wave_speed);


	// TODO 7: Enable the player module
	App->player->Enable();
	// TODO 0: trigger background music
	App->audio->PlayMusic("ken.ogg");
	return true;
}

// UnLoad assets
bool ModuleSceneKen::CleanUp()
{
	LOG("Unloading ken scene");

	App->textures->Unload(graphics);
	App->player->Disable();

	//ship
	delete red_ship;
	red_ship = NULL;

	return true;
}

// Update: draw background
update_status ModuleSceneKen::Update()
{
	// TODO 5: make sure the ship goes up and down
	red_ship->updateWaveCoords();
	// Draw everything --------------------------------------
	// TODO 1: Tweak the parallax movement speed of the sea&sky + flag to your taste
	App->renderer->Blit(graphics, 0, 0, &background, 1.0f); // sea and sky
	App->renderer->Blit(graphics, 560, 8, &(flag.GetCurrentFrame()), 1.0f); // flag animation

	// TODO 3: Draw the ship. Be sure to tweak the speed.
	App->renderer->Blit(graphics, red_ship->coordinates->x, red_ship->coordinates->y, &foreground, 1.0f); // red ship
	
	// TODO 6: Draw the girl. Make sure it follows the ship movement!
	//Girl animation
	App->renderer->Blit(graphics, red_ship->coordinates->x + 192, red_ship->coordinates->y + 95, &(girl_animation.GetCurrentFrame()), 1.0f); // flag animation

	App->renderer->Blit(graphics, 0, 170, &ground);

	// TODO 10: Build an entire new scene "honda", you can find its
	// and music in the Game/ folder

	// TODO 11: Make that pressing space triggers a switch to honda logic module
	// using FadeToBlack module
	//Application.cpp line 89

	return UPDATE_CONTINUE;
}