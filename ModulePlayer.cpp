#include "Globals.h"
#include "Application.h"
#include "ModulePlayer.h"
#include "ModuleInput.h"
#include "ModuleRender.h"
#include "ModuleTextures.h"
#include "SDL/include/SDL.h"



// Reference at https://www.youtube.com/watch?v=OEhmUuehGOA
ModulePlayer::ModulePlayer(bool start_enabled) : Module(start_enabled)
{
	position.x = 100;
	position.y = 116;

	// idle animation (arcade sprite sheet)
	idle.frames.push_back({7, 14, 60, 90});
	idle.frames.push_back({95, 15, 60, 89});
	idle.frames.push_back({184, 14, 60, 90});
	idle.frames.push_back({276, 11, 60, 93});
	idle.frames.push_back({366, 12, 60, 92});
	idle.speed = 0.2f;
	
	// walk backward animation (arcade sprite sheet)
	backward.frames.push_back({542, 131, 61, 87});
	backward.frames.push_back({628, 129, 59, 90});
	backward.frames.push_back({713, 128, 57, 90});
	backward.frames.push_back({797, 127, 57, 90});
	backward.frames.push_back({883, 128, 58, 91});
	backward.frames.push_back({974, 129, 57, 89});
	backward.speed = 0.2f;

	// TODO 8: setup the walk forward animation from ryu4.png
	forward.frames.push_back({ 9, 136, 53, 83 });
	forward.frames.push_back({ 77, 131, 62, 89 });
	forward.frames.push_back({ 160, 128, 67, 92 });
	forward.frames.push_back({ 257, 127, 65, 90 });
	forward.frames.push_back({ 352, 127, 54, 93 });
	forward.frames.push_back({ 432, 130, 52, 90 });
	forward.speed = 0.2f;

	speed = 3;
	current_state = PlayerState::IDLE;
}

ModulePlayer::~ModulePlayer()
{
	// Homework : check for memory leaks
}

// Load assets
bool ModulePlayer::Start()
{
	LOG("Loading player");

	graphics = App->textures->Load("ryu4.png"); // arcade version

	return true;
}

// Unload assets
bool ModulePlayer::CleanUp()
{
	LOG("Unloading player");

	App->textures->Unload(graphics);

	return true;
}

void ModulePlayer::UpdateState()
{
	//Update State
	current_state = PlayerState::IDLE;
	if (App->input->GetKey(SDL_SCANCODE_LEFT) == KEY_REPEAT || App->input->GetKey(SDL_SCANCODE_RIGHT) == KEY_REPEAT) {
		if (App->input->GetKey(SDL_SCANCODE_LEFT) == KEY_REPEAT) {
			current_state = PlayerState::WALKING_BACKWARD;
			position.x -= speed;
		}
			
		if (App->input->GetKey(SDL_SCANCODE_RIGHT) == KEY_REPEAT) {
			current_state = PlayerState::WALKING_FORWARD;
			position.x += speed;

		}
	}


	switch (current_state)
	{
	case PlayerState::IDLE:
		App->renderer->Blit(graphics, position.x, position.y, &(idle.GetCurrentFrame()), 1.0f); // player animation
		break;
	case PlayerState::WALKING_FORWARD:
		App->renderer->Blit(graphics, position.x, position.y, &(backward.GetCurrentFrame()), 1.0f); // player animation
		break;
	case PlayerState::WALKING_BACKWARD:
		App->renderer->Blit(graphics, position.x, position.y, &(forward.GetCurrentFrame()), 1.0f); // player animation
		break;
	default:
		break;
	}
}



// Update
update_status ModulePlayer::Update()
{
	// TODO 9: Draw the player with its animation
	// make sure to detect player movement and change its
	// position while cycling the animation(check Animation.h)
	UpdateState();

	return UPDATE_CONTINUE;
}