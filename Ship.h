#ifndef __SHIP_H__
#define __SHIP_H__
#include "Point.h"
class Ship
{
public:
	int wave_height;
	float wave_speed;
	iPoint *coordinates;
	void updateWaveCoords();
	Ship(int wave_height, float wave_speed);
private:
	bool up = true;
	float currentY;
};
#endif // __SHIP_H__
