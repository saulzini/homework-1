#ifndef __MODULESCENEKEN_H__
#define __MODULESCENEKEN_H__

#include "Module.h"
#include "Animation.h"
#include "Globals.h"
#include "Ship.h"
struct SDL_Texture;


class ModuleSceneKen : public Module
{
public:
	ModuleSceneKen( bool start_enabled = true);
	~ModuleSceneKen();

	bool Start();
	update_status Update();
	bool CleanUp();

public:
	
	SDL_Texture* graphics = nullptr;
	SDL_Rect ground;
	SDL_Rect background;
	SDL_Rect foreground;
	SDL_Rect girl;
	Animation flag;
	Animation girl_animation;
	Ship *red_ship;
	float wave_speed; //ocean waves speed
	int wave_height;
};

#endif // __MODULESCENEKEN_H__