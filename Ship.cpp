#include "Ship.h"

Ship::Ship(int wave_height = 1, float wave_speed=1)
{
	this->currentY = 0;
	this->wave_height = wave_height;
	this->wave_speed = wave_speed;
	this->coordinates = new iPoint(0, 0);
}


void Ship::updateWaveCoords()
{
	if (up) {
		if ( (int)currentY > wave_height) {
			up = false;
			return;
		}
		currentY += wave_speed;
		coordinates->y = (int)currentY;
	}
	else {
		if ( (int)currentY <= 0) {
			up = true;
			return;
		}
		currentY -= wave_speed;
		coordinates->y = (int)currentY;
	}

}
