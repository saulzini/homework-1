#include "Globals.h"
#include "Application.h"
#include "ModuleSceneHonda.h"
#include "ModuleRender.h"
#include "ModuleTextures.h"
#include "ModulePlayer.h"
#include "ModuleInput.h"
#include "ModuleAudio.h"
#include "ModuleFadeToBlack.h"
#include "SDL/include/SDL.h"

ModuleSceneHonda::ModuleSceneHonda(bool start_enabled) : Module(start_enabled)
{
	// background
	background.x = 127;
	background.y = 128;
	background.w = 664;
	background.h = 201;
	
	// ground
	ground_base.x = 75;
	ground_base.y = 375;
	ground_base.w = 846;
	ground_base.h = 66;

	background_sumo.x = 163;
	background_sumo.y = 65;
	background_sumo.w = 337;
	background_sumo.h = 53;

	background_ceiling.x = 92;
	background_ceiling.y = 8;
	background_ceiling.w = 765;
	background_ceiling.h = 47;

	background_item.x = 542;
	background_item.y = 85;
	background_item.w = 33;
	background_item.h = 28;

	mountain.frames.push_back({ 224, 64, 225, 104 });
	mountain.frames.push_back({ 456, 64, 225, 104});
	mountain.speed = 0.08f;

	background_character.frames.push_back({ 688,65,95,103 });
	background_character.frames.push_back({ 793,65,62,104});
	background_character.speed = 0.01f;

}

ModuleSceneHonda::~ModuleSceneHonda()
{

}

bool ModuleSceneHonda::Start()
{
	LOG("Loading honda scene");

	graphics = App->textures->Load("honda_stage2.png");
	graphics2 = App->textures->Load("honda_stage.png");

	App->audio->PlayMusic("honda.ogg");
	return true;
}

update_status ModuleSceneHonda::Update()
{
	App->renderer->Blit(graphics, 0, 170, &ground_base);
	App->renderer->Blit(graphics, 0, 0, &background, 1.0f);
	App->renderer->Blit(graphics, -50, 0, &background_ceiling, 1.0f);
	App->renderer->Blit(graphics2, 183, 56, &(mountain.GetCurrentFrame()), 1.0f); //mountain
	if ((int)background_character.GetCurrentFrameToInt() == 1) {
		App->renderer->Blit(graphics2, 441, 56, &(background_character.GetCurrentFrame()), 1.0f); //character 
	}
	else {
		App->renderer->Blit(graphics2, 408, 56, &(background_character.GetCurrentFrame()), 1.0f); //character 
	}

	App->renderer->Blit(graphics, 180, 135, &background_sumo, 1.0f);
	App->renderer->Blit(graphics, 150, 165, &background_item, 1.0f);
	
	

	return UPDATE_CONTINUE;
}

bool ModuleSceneHonda::CleanUp()
{
	LOG("Unloading honda scene");

	App->textures->Unload(graphics);
	App->player->Disable();
	return true;

}
